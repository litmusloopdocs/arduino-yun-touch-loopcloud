# yun-loopmq-touch

This project is to demonstrate the implementation of MQTT. **Message Queue Telemetry Transport (MQTT)** is an extremely simple and lightweight messaging protocol, designed for constrained devices and low bandwidth, high latency and unreliable networks. The protocol uses **publish/subscribe** communication pattern and is used for machine to machine communication and plays and important role in the internet of things. MQTT works on the **TCP/IP connection**.

**Touch Sensor** enables you to replace press with touch. It can detect the change in capacitance when a finger is near by. That means no matter your finger directly touches the pad or just stays close to the pad, Touch Sensor would outputs HIGH also.
[Click here to find more information about the Touch Sensor.](http://www.seeedstudio.com/wiki/Grove_-_Touch_Sensor)

## Overview


The library provides an example of publish and subscribe messaging with a server that supports MQTT using Arduino yun.
 
***Features provided by the client library:***

* Connect the device to any IP network using Ethernet, Wi-Fi, 4G/LTE
* Publish any message to the MQTT server in standard JSON format on a specific topic 
* Subscribe data from the server to the device on a specific topic
* Unsubscribe the topic to no longer communicate to the device
* Disconnect the device from any network connectivity.


***The following Table shows status of the client and the server when the above functions are implemented:***

> |Function |  Server Status   |    Client Status
> ----------------|------------------|---------------
|Looopmq.connect | Connected       | Connected|
|Loopmq.publish |Connected |  Connected|
|Loopmq.subscribe | Connected | Connected|
|Loopmq.unsubscribe | Connected | Disconnected|
|Loopmq.disconnect |  Disconnected |  Disconnected|


## Getting Started With Ardunio Yun


**Basic steps to connect YUN to internet are given below:**

>1. Connect the YUN to laptop with USB cable
2. In your network manager, connect to the network with the name like Arduin Yun-B4218AF847E6 or Linino-B4218AF847E6
3. Go to the web address arduino\local or ip `192.168.240.1`
4. If connected to *Arduino* password is ***arduino*** and if *Linino*  network is connected then default password is ***doghunter***
5. Once logged in, Go to configure Wifi and enter the SSID and the passkey to connect to the network.
6. Click restart to configure the device and your YUN will be connected to the the internet via Wi-Fi

>**Note:**  
*To **restart** the **AR9331**, which reboots OpenWrt-Yun, press the "YÚN RST" reset button that is close to the analog inputs pins and the LEDs of the board.*
*To **restart** the the **32U4** and restart the currently installed Arduino sketch, tap the button next to the Ethernet port two times.*
*The **reset** button for **WiFi** is located next to the USB-A connector. It is labeled "WLAN RST". When you press the button, the WLAN LED will flash.*
*If you move to a different network and can no longer wirelessly access the Yún through its web interface, you can reset the Yún's network configuration by pressing the WiFi reset button (WLAN RST) for longer longer than 5 seconds, but less than 30, the AR9331 processor will **reboot.** The WiFi configuration will be reset and the Yún will start its own wiFi network Arduino Yún-XXXXXXXXXXXX. Any other modification/configuration will be retained.*
*To **reset** the **OpenWrt-Yun** distribution to its default state, press the WiFi reset button (WLAN RST) for at least 30 seconds. The board reverts to the original settings: like just taken out of the box or to the latest update of the OpenWRT image you have reflashed before. Among other things, this removes all installed files and network settings.*

**Steps to connect the board and then send data to MQTT are as below:**

>1. Assemble and connect the board as shown 
![alt text](https://bytebucket.org/litmusloopdocs/arduino-yun-touch-loopcloud/raw/master//extras/touch_sensor_initial_setup.png)
2. Install Arduino IDE and select YUN from boards and the COM port it is connected to
3. Install the library or open the *yun_loopmq_touch_sensor_with_LED.ino* file from the examples.
4. Enter the MQTT broker details in the *configuration.h* file present along with the *.ino* file.
5. Once code is compiled and flashed to yun, you should see the messages published by you to the device. The Led Status of the yun will be as shown in the figure 
![alt text](https://bytebucket.org/litmusloopdocs/arduino-yun-touch-loopcloud/raw/master//extras/MQTT_STATUS.png)


**Steps to test MQTT connection (if required) can be found under /repo/extras/testmqqt.md :**
***Note***: *If you are not using Google Chrome as your default browser, download **MQTTSpy** to test MQTT connection.*


## Configuration

The user need to define a list of parameters in order to connect the device to a server in a secured manner.

**Below are the list of minimum definitions required by the user to send data to the cloud:**

```
#define port_number 1883                             // Port number
#define server "loopdocker1.cloudapp.net"            // Server name
#define clientID "yunclient"                         // ClientID
#define password "password"                          // password
#define userID "admin"                               // username 
#define subTOPIC "arduino_yun/touch"                 // Subscribe on this topic to get the data
#define pubTopic "arduino_yun/loop2"                 // Publish on this tpoic to send data or command to device 
```

## Functions

1.***loopmq.connect (client ID)***

This function is used to connect the device or the client to the client ID specified by the user.

```
if (loopmq.connect(c)) {
      Serial.println ("connected");
```       


2.***loopmq.connect (client ID, username, password)***

Checks for the username and password specified by the user to connect the device to the network.

```
if (loopmq.connect(c, user, pass))
  loopmq.publish(p,buffer);                 // Publish message to the server once only
```
3.***loopmq.publish (topic, data)***

This function is used to publish data in string format to the topic specified by the user. 

```
Loopmq.publish (p,buffer);                   // Publish message to the server
```

4.***loopmq.subscribe (topic)***

This function is used to subscribe to a topic to which data will be published from the user to the     device. 

```
loopmq.subscribe(s);                          // Subscribe to a topic
```

5.***loopmq.unsubscribe (topic)***

This function is used to unsubscribe the device from the server. Calling this function will stop sending messages from the device to the server.

```
// loopmq.unsubscribe(s);                     // Note: uncomment the code to unsubscribe from the topic
```
 
6.***lopmq.disconnect ()***

This function is used to disconnect the device from the server. Disconnect does not stop the functionality of the device but disconnects it from the network. The device works fine locally but does not send any update to the internet.

```
// loopmq.disconnect();                       // Note: uncomment the code to disconnect the device
```

7.***loop ()***

This function is the sensor code for the Touch sensor.

```
 if(sensorValue==1)
  {
  digitalWrite(ledPin,HIGH);
  delay(1000);
  Serial.print("Sensor value is ");
  Serial.print(sensorValue);
  Serial.print(',');
  Serial.print("LED Status is ");
  Serial.print(ledstatus());
  Serial.println();
  }
  else
  {
  digitalWrite(ledPin,LOW);
  delay(1000);
  Serial.print("Sensor value is ");
  Serial.print(sensorValue);
  Serial.print(',');
  Serial.print("LED Status is ");
  Serial.print(ledstatus());
  Serial.println();
  }
```

8.***JSON PARSER***

This function is used to create a JSON payload to be passed to the broker as payload. Please refer the [link](https://github.com/bblanchon/ArduinoJson/wiki/Compatibility-issues) for any compalibility issues.

```
StaticJsonBuffer<200> jsonBuffer;                 //  Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase that value. 

  JsonObject& root = jsonBuffer.createObject();   // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "Touch Sensor with LED";      // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;

  JsonObject& data= root.createNestedObject("data"); // nested JSON 
  data["Touch Sensor"]= sensorValue;                 // Add data["key"]= value
  data["LED Status"]=ledStatus;


  root.printTo(Serial);                               // prints to serial terminal
  Serial.println();

  char buffer[100];                                   // buffer to pass as payload
  root.printTo(buffer, sizeof(buffer));               // copy the JSON to the buffer to pass as a payload 
```